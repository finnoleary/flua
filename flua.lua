local unpack = table.unpack or unpack
function map(list, f)
	local t = {}
	for _, v in pairs(list) do
		table.insert(t, f(v) or nil)
	end
	return t
end
function filter(list, f)
	local t = {}
	for _, v in pairs(list) do
		local r = f(v)
		if r then table.insert(t, v) end
	end
	return t
end
function apply(t)
	local f = table.remove(t, 1)
	return f(unpack(t))
end
function cond(...)
	local p = {...}
	_do = function (v)
		if type(v) ~= "table" then return v end
		return apply(v)
	end

	for i = 1, #p, 2 do
		if i == #p then return _do(p[i]) end
		if _do(p[i]) then return _do(p[i+1]) end
	end
end